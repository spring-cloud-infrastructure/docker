#!/bin/bash

function checkout_project() {
  local PROJECT=$1
  local PROJECT_DIRECTORY=/projects/$PROJECT

  if [ -d $PROJECT_DIRECTORY ]; then
    echo Project $PROJECT already exists, pulling the latest changes
    git -C $PROJECT_DIRECTORY pull -r

  else
    echo Clonning project $PROJECT
    git clone https://gitlab.com/spring-cloud-infrastructure/$PROJECT.git $PROJECT_DIRECTORY
  fi
}

for PROJECT in documentation commons/spring-cloud-starter-version-aware-service-discovery-client commons/spring-cloud-gradle-plugin commons/spring-cloud-starter-resource-server commons/spring-cloud-starter-quartz-actuator config-repo config-server api-gateway service-discovery authorization-server spring-boot-admin scheduler-service service-a service-b; do
  checkout_project $PROJECT
done

chown -R $(whoami) /projects
chmod -R 777 /projects
