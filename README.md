# Spring Cloud Infrastructure Docker

Docker images + scripts + all needed for running the Spring Cloud Infrastructure project.

## 1. Check out projects

```shell script
docker-compose \
  -f ./checkout/docker-compose.yml \
  up
```

## 2. Build projects

```shell script
docker-compose \
  -f ./build/docker-compose.yml \
  up
```

## 4. Configuration

- [.env](./run/.env) - Docker environment variables (mainly username and password pairs)
- [pipelines.yml](./run/logstash/pipelines.yml) - Logstash pipelines
- [config-repo](https://gitlab.com/spring-cloud-infrastructure/config-repo/-/blob/master/README.md) - Spring
  Configuration store repository (configuration files for Spring applications, SSL stores)

## 5. Run containers

### PROD mode

```shell script
docker-compose \
  -f ./run/docker-compose.yml \
  up -d
```

### DEBUG mode

```shell script
docker-compose \
  -f ./run/docker-compose.yml \
  -f ./run/docker-compose.override.debug.yml \
  up -d
```

Check out [run/docker-compose.override.debug.yml](./run/docker-compose.override.debug.yml) file for either an info about
a particular service's port (`BPL_DEBUG_PORT`) or making a service waiting for a debugger to connect by
changing `BPL_DEBUG_SUSPEND` from `'false'` to `'true'`.

## 6. Testing

Check out [config-repo/README.md#SSL](https://gitlab.com/spring-cloud-infrastructure/config-repo#ssl) project for SSL
certificates as well as a guide on how to use them.

| Spring Service       | Local Port | Debug address | API Gateway                        |
|----------------------|------------|---------------|------------------------------------|
| config-server        | 8888       | 10.0.0.1:9888 | https://10.0.0.1/config-server     |
| service-discovery    | 8761       | 10.0.0.1:9761 | N/A                                |
| api-gateway          | 8081       | 10.0.0.1:9081 | https://10.0.0.1/                  |
| authorization-server | 8082       | 10.0.0.1:9082 | https://10.0.0.1/auth              |
| spring-boot-admin    | 8083       | 10.0.0.1:9083 | https://10.0.0.1/spring-boot-admin |
| schedule-service     | 8084       | 10.0.0.1:9084 | N/A                                |
| service-a            | 8091       | 10.0.0.1:9091 | https://10.0.0.1/service-a         |
| service-b            | 8092       | 10.0.0.1:9092 | https://10.0.0.1/service-b         |
