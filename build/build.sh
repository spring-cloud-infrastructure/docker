#!/bin/bash

GRADLE_WRAPPER_VERSION=$1
PROJECT_CACHE_DIRECTORY=$2

function update_gradle_wrapper() {
  local PROJECT=$1
  local PROJECT_DIRECTORY=/projects/$PROJECT
  echo Updating gradle wrapper version of $PROJECT to $GRADLE_WRAPPER_VERSION
  $PROJECT_DIRECTORY/gradlew --project-dir $PROJECT_DIRECTORY wrapper --gradle-version $GRADLE_WRAPPER_VERSION
}

function build_docker_image() {
  local PROJECT=$1
  local PROJECT_DIRECTORY=/projects/$PROJECT

  echo Building a docker image for $PROJECT
  $PROJECT_DIRECTORY/gradlew --project-dir $PROJECT_DIRECTORY clean bootBuildImage --warning-mode all --project-cache-dir=$PROJECT_CACHE_DIRECTORY
}

function grand_permissions_to_directories() {
  local PROJECT_DIRECTORY=/projects/$1

  for DIRECTORY in build .gradle; do
    chown -R $(whoami) $PROJECT_DIRECTORY/$DIRECTORY
    chmod -R 777 $PROJECT_DIRECTORY/$DIRECTORY
  done
}

for SERVICE in config-server api-gateway service-discovery authorization-server spring-boot-admin scheduler-service service-a service-b; do
  update_gradle_wrapper $SERVICE
  build_docker_image $SERVICE
  grand_permissions_to_directories $SERVICE
done
